import random
import texts

limit_event_types = 4
limit_events = 4
max_step_number = 7

spacesuit_states = texts.spacesuit_states_texts
energy_weapon_states = texts.energy_weapon_states_texts
water_bottle_states = texts.water_bottle_states_texts
food_container_states = texts.food_container_states_texts


class Hero:
    """
    class for hero :)
    """
    inventory = {
        'spacesuit': [texts.inventory_texts['spacesuit'], True, spacesuit_states[0]],
        'energyWeapon': [texts.inventory_texts['energyWeapon'], True, energy_weapon_states[0]],
        'waterBottle': [texts.inventory_texts['waterBottle'], True, water_bottle_states[0]],
        'foodContainer': [texts.inventory_texts['foodContainer'], True, food_container_states[0]]
    }


class Story:
    """
    class for story
    """
    step = 0  # game step
    max_step_number = 0  # max number of game steps

    game_over = False

    active_event = None  # current active event
    active_scene = None  # current active event part (away / lose / attack...)

    current_scene_part = 0

    is_a_battle = False

    list_of_command = texts.list_of_command_texts

    def __init__(self) -> object:
        self.max_step_number = max_step_number
        self.main_hero = Hero()

        self.default_state = texts.default_state

        self.events = [[texts.nothing_happens_default,
                        texts.nothing_happens_01,
                        texts.nothing_happens_02,
                        texts.nothing_happens_03,
                        texts.nothing_happens_04
                        ],
                       [texts.incident_01,
                        texts.incident_02
                        ],
                       [texts.sand_predator_attack,
                        texts.sand_monster_attack
                        ]
                       ]

    @staticmethod
    def is_the_end_of_scene(self):
        return self.current_scene_part >= len(self.active_scene) - 1

    @staticmethod
    def get_correct_event_type(self, event_type) -> int:
        return event_type if len(self.events) > event_type else 0

    @staticmethod
    def get_correct_event_number(self, event_type, event_number) -> int:
        return event_number if len(self.events[event_type]) > event_number else 0

    def get_random_event(self):
        event_type = self.get_correct_event_type(self, random.randint(0, limit_event_types))
        event_number = self.get_correct_event_number(self, event_type, random.randint(0, limit_events))
        return self.events[event_type][event_number]

    def next_step(self):
        if self.step == 0:                                               # start the game
            self.active_event = self.default_state
            self.current_scene_part = 0
            self.active_scene = self.active_event.start_messages

            self.game_over = False

            self.step += 1

            return texts.welcome_message, False

        elif self.step == self.max_step_number:                          # finish the game
            self.game_over = True
            return texts.victory_message, False

        else:
            if self.is_a_battle:                                         # user decided to leave the battle
                self.current_scene_part = 0
                self.active_scene = self.active_event.lose_messages

                if self.is_the_end_of_scene(self):
                    self.is_a_battle = False
                    need_next_keyboard = False
                else:
                    need_next_keyboard = True

                return self.active_scene[0], need_next_keyboard
            else:                                                        # usual move to the next event
                self.active_event = self.get_random_event()
                self.current_scene_part = 0
                self.active_scene = self.active_event.start_messages

                self.step += 1

                self.is_a_battle = self.active_event.is_a_battle

                return self.active_scene[0], not self.is_the_end_of_scene(self)

    def continue_scene(self):
        if self.current_scene_part < len(self.active_scene)-1:
            self.current_scene_part += 1

            if self.is_the_end_of_scene(self):
                self.is_a_battle = False
                need_next_keyboard = False
                self.game_over = self.active_scene == self.active_event.lose_messages
            else:
                need_next_keyboard = True

            return self.active_scene[self.current_scene_part], need_next_keyboard
        else:  # incorrect user action
            return texts.next_action_message, False

    def lose_the_game(self):
        self.active_scene = self.active_event.lose_messages
        self.current_scene_part = 0
#        self.active_event = self.default_state

        if self.is_the_end_of_scene(self):
            self.is_a_battle = False
            need_next_keyboard = False
            self.game_over = True
        else:
            need_next_keyboard = True

        return self.active_scene[0], need_next_keyboard

    def attack(self):
        self.current_scene_part = 0
        self.active_scene = self.active_event.attack_messages
        self.is_a_battle = False
        return self.active_scene[0], not self.is_the_end_of_scene(self)

    def inspect_inventory(self):
        return_string = self.active_event.inventory_messages[0]

        if not self.is_a_battle:
            for item in self.main_hero.inventory.keys():
                if self.main_hero.inventory[item][1]:
                    return_string += '\n' + '- ' + self.main_hero.inventory[item][0] + ''\
                                     + ' _(' + self.main_hero.inventory[item][2] + ')_'

        return return_string, False
