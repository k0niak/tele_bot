import telebot
import engine

bot = telebot.TeleBot('945272699:AAFcD1SyK0efc189vKDi-gAvH3WQBkrERwk')

game = engine.Story()

# keyboards #
game_keyboard_default = telebot.types.ReplyKeyboardMarkup(True, True)
i = 1
while i < len(game.list_of_command):
    game_keyboard_default.row(game.list_of_command[i])
    i += 1

game_keyboard_next = telebot.types.ReplyKeyboardMarkup(True, True)
game_keyboard_next.row(game.list_of_command[0])


@bot.message_handler(commands=['help'])
def start_message(message):
    bot.send_message(message.chat.id, '''
*Поддерживаемые команды:*
/help - показать подсказку
/start - начать квест
''', parse_mode='Markdown')


@bot.message_handler(commands=['start'])
def start_message(message):
    game.step = 0
    game_message = game.next_step()
    bot.send_message(message.chat.id, game_message, reply_markup=game_keyboard_default, parse_mode='Markdown')


@bot.message_handler(content_types=['text'])
def send_text(message):

    if message.text.lower() == game.list_of_command[0].lower():  # 'Далее...',
        game_message = game.continue_scene()

    elif message.text.lower() == game.list_of_command[1].lower():  # 'Осмотреть инвентарь',
        game_message = game.inspect_inventory()

    elif message.text.lower() == game.list_of_command[2].lower():  # 'Двигаться вперед',
        game_message = game.next_step()

    elif message.text.lower() == game.list_of_command[3].lower():  # 'Отчаяться и сдаться',
        game_message = game.lose_the_game()

    elif message.text.lower() == game.list_of_command[4].lower():  # 'Вперед, в атаку!'
        game_message = game.attack()

    else:
        bot.send_message(message.chat.id, 'Данная команда не поддерживается!')
        return

    if not game_message == '':
        if not game.game_over:
            bot.send_message(message.chat.id, game_message[0],
                             reply_markup=game_keyboard_next if game_message[1] else game_keyboard_default,
                             parse_mode='Markdown')
        else:
            bot.send_message(message.chat.id, game_message[0], parse_mode='Markdown')


bot.polling()
