spacesuit_states_texts = (
    'Цел',
    'Изрядно потрепан, но герметичен',
    'Разгерметизирован'
)
energy_weapon_states_texts = (
    'Заряжено',
    'Наполовину заряжено',
    'Разряжено'
)
water_bottle_states_texts = (
    'Полон',
    'Наполовину полон. Кто сказал стакан?',
    'Пуст'
)
food_container_states_texts = (
    'Заполнена на 2/3. Кто-то слишком много ест!',
    'Есть еще немного еды',
    'Пуста')


inventory_texts = {
    'spacesuit': 'скафандр',
    'energyWeapon': 'энергоружье',
    'waterBottle': 'баллон с питьевой водой',
    'foodContainer': 'емкость с пищевой массой'
}


list_of_command_texts = [
    'Далее...',
    'Осмотреть инвентарь',
    'Двигаться вперед',
    'Отчаяться и сдаться',
    'Вперед, в атаку!'
]


welcome_message = '''
Во время полета к своей базе на планете П-71, Ваш "Дребезжабль" сломался и рухнул в пустынной местности.
Всю планету П-71 можно назвать "пустынной местностью", зачем тут база и Вы - не понятно.

После часа, а может полутора, лежки в отключке, Вы очнулись. Вам необходимо как можно скорее добраться до своей базы.
Запасы еды и воды ограничены. По Вашей оценке их должно впритык хватить на весь путь, если, конечно, ничего не
случиться...
'''

victory_message = '''
Поздравляю! Вы пришли к свое базе! Вы герой! Никто на этой планете Вас не забудет! Хотя тут особо никого и нет ;)
'''

inventory_message = '*При себе Вы имеете:*'

inventory_message_in_attack = 'Вы сумасшедший? Вам грозит смертельная опасность, а Вы хотите пошарить в карманах?!'

die_message = 'Вы погибли под палящим солнцем от жажды и истощения. Эх...'

no_attack_message = 'Не тратьте впустую силы. Здесь не на кого нападать.'

next_action_message = 'Выберите следующее действие'


class EventMessages:
    start_messages = []
    inventory_messages = []
    away_massages = []
    lose_messages = []
    attack_messages = []

#    event_is_not_over = False
#    current_event_part = 0
    is_a_battle = False


# Default #
default_state = EventMessages()
default_state.start_messages = [next_action_message]
default_state.inventory_messages = [inventory_message]
default_state.away_massages = []
default_state.lose_messages = [die_message]
default_state.attack_messages = [no_attack_message]

# Nothing happens #
nothing_happens_default = EventMessages()
nothing_happens_default.start_messages = ['Вы идете по пустынной местности...']
nothing_happens_default.inventory_messages = [inventory_message]
nothing_happens_default.away_massages = []
nothing_happens_default.lose_messages = [die_message]
nothing_happens_default.attack_messages = [no_attack_message]

nothing_happens_01 = EventMessages()
nothing_happens_01.start_messages = ['Вы все еще идете по пустынной местности...']
nothing_happens_01.inventory_messages = [inventory_message]
nothing_happens_01.away_massages = []
nothing_happens_01.lose_messages = [die_message]
nothing_happens_01.attack_messages = [no_attack_message]

nothing_happens_02 = EventMessages()
nothing_happens_02.start_messages = ['Таки, Вы идете по пустынной местности...']
nothing_happens_02.inventory_messages = [inventory_message]
nothing_happens_02.away_massages = []
nothing_happens_02.lose_messages = [die_message]
nothing_happens_02.attack_messages = [no_attack_message]

nothing_happens_03 = EventMessages()
nothing_happens_03.start_messages = ['... пустынная местность, будь она не ладна...']
nothing_happens_03.inventory_messages = [inventory_message]
nothing_happens_03.away_massages = []
nothing_happens_03.lose_messages = [die_message]
nothing_happens_03.attack_messages = [no_attack_message]

nothing_happens_04 = EventMessages()
nothing_happens_04.start_messages = ['... и чего мне дома не сиделось...']
nothing_happens_04.inventory_messages = [inventory_message]
nothing_happens_04.away_massages = []
nothing_happens_04.lose_messages = [die_message]
nothing_happens_04.attack_messages = [no_attack_message]

# incidents #
incident_01 = EventMessages()
incident_01.start_messages = ['Вы идете мимо камней.']
incident_01.inventory_messages = [inventory_message]
incident_01.away_massages = []
incident_01.lose_messages = [die_message]
incident_01.attack_messages = [no_attack_message]

incident_02 = EventMessages()
incident_02.start_messages = ['Вы нашли один из обломков Вашего (или не Вашего?) корабля.']
incident_02.inventory_messages = [inventory_message]
incident_02.away_massages = []
incident_02.lose_messages = [die_message]
incident_02.attack_messages = [no_attack_message]

# attacks #
sand_monster_attack = EventMessages()
sand_monster_attack.is_a_battle = True
sand_monster_attack.start_messages = ['''
Вы видите перед собой воронку. Возможно она образовалась от падения метеорита на поверхность планеты.
Вдруг Вы замечаете, что в центре воронки что-то шевелится, подойдя ближе Вы теряете равновесие на сыпучем песке и
падаете в воронку.
''', '''
Центр воронки раскрывается как будто это чья-то пасть! Подождите, эта планета же безжизненна! Не-е-е-е-ет!!!
''', '''
В последний момент вы хватаетесь за торчащий камень и удерживаетесь на нем.
''']
sand_monster_attack.inventory_messages = [inventory_message_in_attack]
sand_monster_attack.away_massages = ['''
Вы пытаетесь вскарабкаться наверх изо всех сил, но песок под Вами очень сыпуч.
В какой-то момент Ваша рука срывается с камня и Вы летите прямиком в пасть чудовища...
''', '''
Эх, не везет, так не везет... Прощайте...
''']
sand_monster_attack.lose_messages = ['Вы отпускаете камень и Вы летите прямиком в пасть чудовища.',
                                     'Героем Вас точно не назовут. Ну хотя бы зверька покормили...']
sand_monster_attack.attack_messages = ['''
Вы отчаянно палите из своего энергоружья прямо в пасть чудовищу. Вся воронка начинает как-будто вибрировать
или трястись. Внезапно чудовище как будто чихает или поди разбери, что делает, и Вы улетаете вместе с облаком
песка далеко от места битвы.
''', '''
Придя в себя и отряхнувшись от песка и пыли Вы начинаете себя осматривать.
Скафандр изрядно потрепан, и такое чувство, как будто Вы наложили в штаны. Нет! Проклятье! Емкость с пищевой
массой пробита и вся масса вытекает внутрь скафандра!

Делать нечего, нужно скорее двигаться дальше.
''']


sand_predator_attack = EventMessages()
sand_predator_attack.is_a_battle = True
sand_predator_attack.start_messages = ['''
Боковым зрением Вы как будто видите какое-то движение позади. Внезапно со спины на Вас нападает песчаный хищник!
''', '''
Чудом увернувшись от удара, Вы оказываетесь напротив хищника. Животное готовиться к следующему смертельному прыжку.
''']
sand_predator_attack.inventory_messages = [inventory_message_in_attack]
sand_predator_attack.away_massages = ['''
Вы бросаетесь изо всех сил наутек. Тяжелый скафандр (последнюю облегченную модель Вам так и не дали) не позволяет
бежать с необходимой скоростью. Песчаный хищник в несколько прыжков настигает Вас и...
''', '''
Эх, не везет, так не везет... Прощайте...
''']
sand_predator_attack.lose_messages = ['Хищник бросается на Вас и...',
                                      'Приятного аппетита чудный зверек!']
sand_predator_attack.attack_messages = ['''
Хищник бросается на Вас! Вы отчаянно палите из своего энергоружья. Животное вопит от боли, приземляется на Вас,
сбивая Вас с ног. Затем вскакивает и убегает прочь.
''', '''
Победа! Но баллон с водой пробит и вода вытекает внутрь скафандра струйками по ногам. Ощущения как в детстве,
когда еще не умел проситься на горшок...
Делать нечего, нужно скорее двигаться дальше.
''']
